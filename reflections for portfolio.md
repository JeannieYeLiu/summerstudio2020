?. Challenges faced this week
?. Reflection & Reflection on SLO
?. next week plan and relation between project more in depth
?. Title with minimal info is not required
?. Fix up third week

# SLO
1. Apply design and systems thinking to respond to a defined or newly identified problem.
1. Engage with stakeholders to identify a problem or scope a defined problem.
3. Apply technical skills to develop, model and/or evaluate designs.
4. Demonstrate effective collaboration and communication skills.
5. Conduct critical self, peer, and group review and performance evaluation. 
~ proof of collaboration


CILOs:

identify, engage, interpret and analyse stakeholder needs and cultural perspectives
peer review/ group review
performance evaluation

Artefacts might include:
1. Exploration of the problem, using a range of qualitative and quantitative research methods, such as
mindmaps, sketches, research articles, plans, commentary etc. 
3
2. Discussions with stakeholders
3. Feedback notes and debriefs
4. Articulating a unique and concise design problem
5. Idea generation, developing many potential solutions
6. Prototypes developed (e.g. photos, video)
7. Test prototype for refinement
8. Pitches for the proposal (e.g. PPT slides) 

Final reflection: achievenment of the learning outcome


# 1st week (submission 1 )
* introduction 
~~~
* photo
* statement of motivation to take studio
* objectives in taking the subject
~~~

* Learning contract:
~~~
* what intend to achieve
* learning goals
* task to be completed
* contribution to group effort
* what will i learni in process
* what i will produce
~~~


# 1-4 submissions need to: 
* Curated artefacts (at least 2 submission)
~~~
* engagement with each submission across 5 SLO
* developement with each submission across 5 SLO
~~~
* Reflective statement
~~~
* demonstrate engagement with all studio teaching and learning activities, include:
*   - evaluation and/or incorporation of expert, facilitator & peer feedback
*   - provision of thoughtful, informed feedback to peers, include: 
*       - systematic group progress review each week
*   - revised version of learning contract, include:
*       - intended learning  objectives modified by development of concept in the studio
*       
~~~
learning contract
{what indended to achieve, learning goal, task to be completed, technical skills included}


# 5-6 th week(submission 4/ final submission) / (In this senarios 5th submission)
* conclusion
~~~
* articulating how your learning journey has enabled you to meet/redefine your objectives,
  * described in your learning contract
~~~

* final reflective statement
~~~
* systhesis learning journey
* assess each SLO, indicating the ways your learning contract was achieved
* Reflectively curated, relevant artefacts that
    * 1.form a coherent and cumulative substantiation of claims
of achievement of the subject’s five SLO. 
~~~

* final reflection {development & achievenment of SLO throughout summer studio from each week; } 

