from tensorflow.compat.v1.lite import TFLiteConverter
import tensorflow as tf

# So in this pipeline - I did not specify the input details of the model
# So I rely on the old tensorflow lite converter as I can specify that here

converter = TFLiteConverter.from_keras_model_file('hotdog.h5',
    input_shapes={"input_1":[1, 299, 299, 3]})
converter.optimizations = [tf.lite.Optimize.OPTIMIZE_FOR_LATENCY]
tflite_model = converter.convert()

open('hotdog.tflite', 'wb').write(tflite_model)
print('Model has been converted')
