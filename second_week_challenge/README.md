# Second Week Challenge 

For this challenge you are required to design a CNN to determine whether an item is a hot dog or not a hot dog. 

You have been provided with a dataset that has been sorted into training, validation and testing sets.

# Instructions

You have been provided with the basic set up for you neural network and will need to complete the rest of the network in order to complete the challenge.

You are required to fill out the incomplete sections within the skeleton code.
Instruction are provided for you within the script

You may need to look up a few of the parameters for the model layers. 

Use Benny's tutorial code from the previous lesson for guidance. 

You have also been given the code to test your model with the test dataset. 


Have fun and good luck :) 


